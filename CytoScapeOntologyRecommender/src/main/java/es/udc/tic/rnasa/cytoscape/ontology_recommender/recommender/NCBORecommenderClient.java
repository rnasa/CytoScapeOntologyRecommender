/**
 * 
 */
package es.udc.tic.rnasa.cytoscape.ontology_recommender.recommender;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/*
Recommender – REST API documentation (http://data.bioontology.org/documentation#nav_recommender)
Recommender
	The Recommender endpoint receives a text or a list of keywords and suggests appropriate ontologies for it.	
  
Recommend
	[GET] /recommender?input={input text or keywords}
	example: /recommender?input=Melanoma+is+a+malignant+tumor+of+melanocytes+which+are+found+predominantly+in+skin+but+also+in+the+bowel+and+the+eye.

Parameters:
	input_type = {1|2} // default = 1. 
		1 means that the input type is text. 
		2 means that the input type is a list of comma separated keywords.
	output_type = {1|2} // default = 1. 
		1 means that the output will be a ranked list of individual ontologies. 
		2 means that the output will be a ranked list of ontology sets.
	max_elements_set = {2|3|4} // default = 3. 
		Maximum number of ontologies per set (only for output_type = 2).
	wc = value in the range [0, 1] // default = 0.55. Weight assigned to the ontology coverage criterion.
	ws = value in the range [0, 1] // default = 0.15. Weight assigned to the ontology specialization criterion.
	wa = value in the range [0, 1] // default = 0.15. Weight assigned to the ontology acceptance criterion.
	wd = value in the range [0, 1] // default = 0.15. Weight assigned to the ontology detail criterion.
	ontologies = {ontology_id1, ontology_id2, ..., ontology_idN} // default = (empty) (all BioPortal ontologies will be evaluated).
*/


/**
 * Wrapper class for NCBO Recommender REST API service.
 * REST API documentation (http://data.bioontology.org/documentation#nav_recommender)
 * The Recommender endpoint receives a text or a list of keywords and suggests appropriate ontologies for it.	
 * @author Alberto Alvarellos González
 */
public class NCBORecommenderClient
{
	private final String	recommenderScheme	= "http";
	private final String	recommenderHost		= "data.bioontology.org";
	private final String	recommenderPath		= "/recommender";
	private final String	apikey				= "6ad71d2c-e4a7-45af-b4cd-ec410814913d";

	/**
	 * Creates a default NCBO Recommender Client
	 */
	public NCBORecommenderClient()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * Takes as input a text or a list of keywords and suggests appropriate ontologies for it using an http get method
	 * 
	 * @return a ranked list of ontologies in text format
	 * @throws UnsupportedEncodingException
	 *             if the the Character Encoding for the given text is not supported
	 */
	public String getRecommendedOntologies(String text) throws UnsupportedEncodingException
	{
		// TODO check "text" parameter for correctness and avoiding hacks
		// TODO improve exception handling

		String recommendedOntologies = null;
		HttpGet httpGet = null;

		try
		{
			// Build the uri that will be used with http get method
			URIBuilder URIBuilder = new URIBuilder();
			URIBuilder = URIBuilder.setScheme(recommenderScheme);
			URIBuilder = URIBuilder.setHost(recommenderHost);
			URIBuilder = URIBuilder.setPath(recommenderPath);
			URIBuilder = URIBuilder.setParameter("apikey", apikey);
			URIBuilder = URIBuilder.setParameter("input", text);

			// TODO use all parameters
//			Parameters:
//				input_type = {1|2} // default = 1. 
//					1 means that the input type is text. 
//					2 means that the input type is a list of comma separated keywords.
//				output_type = {1|2} // default = 1. 
//					1 means that the output will be a ranked list of individual ontologies. 
//					2 means that the output will be a ranked list of ontology sets.
//				max_elements_set = {2|3|4} // default = 3. 
//					Maximum number of ontologies per set (only for output_type = 2).
//				wc = value in the range [0, 1] // default = 0.55. Weight assigned to the ontology coverage criterion.
//				ws = value in the range [0, 1] // default = 0.15. Weight assigned to the ontology specialization criterion.
//				wa = value in the range [0, 1] // default = 0.15. Weight assigned to the ontology acceptance criterion.
//				wd = value in the range [0, 1] // default = 0.15. Weight assigned to the ontology detail criterion.
//				ontologies = {ontology_id1, ontology_id2, ..., ontology_idN} // default = (empty) (all BioPortal ontologies will be evaluated).
	
			URI uri = URIBuilder.build();
			httpGet = new HttpGet(uri);
		}
		catch(URISyntaxException e1)
		{
			// TODO Use Cytoscape logging system
			e1.printStackTrace();
		}

		// TODO try-with-resources used, so httpclient is autoclosed; test performance reusing httpclient through several calls and test multithreading
		try(CloseableHttpClient httpclient = HttpClients.createDefault(); CloseableHttpResponse response = httpclient.execute(httpGet);)
		{
			// TODO write a custom “response handler” to handle server status codes properly
			System.out.println(response.getStatusLine());

			// TODO do something useful with the response body and ensure it is fully consumed
			HttpEntity entity = response.getEntity();
			recommendedOntologies = entity != null ? EntityUtils.toString(entity) : null;
		}
		catch(ClientProtocolException e)
		{
			// TODO Use Cytoscape logging system
			e.printStackTrace();
		}
		catch(IOException e) // When response close method called by try-with-resources fails
		{
			// TODO Use Cytoscape logging system
			e.printStackTrace();
		}

		return recommendedOntologies;
	}
}
