package es.udc.tic.rnasa.cytoscape.ontology_recommender;

import java.io.UnsupportedEncodingException;

import es.udc.tic.rnasa.cytoscape.ontology_recommender.recommender.NCBORecommenderClient;

/**
 * @author Alberto Alvarellos González
 *
 */
public class AppTest 
{
    public static void main( String[] args ) throws UnsupportedEncodingException
    {
        NCBORecommenderClient recommender = new NCBORecommenderClient();
        String text = "Melanoma is a malignant tumor of melanocytes which are found predominantly in skin but also in the bowel and the eye";
        
        //Test get method
        String resultWithGet = recommender.getRecommendedOntologies(text);
        System.out.println("Recommended Ontologies, get method:");
        System.out.println(resultWithGet);
        
        //Test post method
        String resultWithPost = recommender.getRecommendedOntologies(text);
        System.out.println("Recommended Ontologies, post method:");
        System.out.println(resultWithPost);
    }
}
