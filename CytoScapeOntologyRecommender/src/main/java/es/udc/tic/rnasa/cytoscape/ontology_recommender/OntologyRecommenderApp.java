/**
 * 
 */
package es.udc.tic.rnasa.cytoscape.ontology_recommender;

import org.cytoscape.app.swing.AbstractCySwingApp;
import org.cytoscape.app.swing.CySwingAppAdapter;

/**
 * @author Alberto Alvarellos González
 *
 */
public class OntologyRecommenderApp extends AbstractCySwingApp
{

	/**
	 * @param swingAdapter
	 */
	public OntologyRecommenderApp(CySwingAppAdapter swingAdapter)
	{
		super(swingAdapter);
		//Use the Ontology Recommender Menu Action
		swingAdapter.getCySwingApplication().addAction(new MenuAction(adapter));
	}
}
