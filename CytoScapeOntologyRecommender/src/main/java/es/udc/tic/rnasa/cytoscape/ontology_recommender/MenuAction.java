/**
 * 
 */
package es.udc.tic.rnasa.cytoscape.ontology_recommender;

import java.awt.event.ActionEvent;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;

/**
 * @author Alberto Alvarellos González
 *
 */
public class MenuAction extends AbstractCyAction
{
	private static final long	serialVersionUID	= 1L;
	private final CyAppAdapter	adapter;

	public MenuAction(CyAppAdapter adapter)
	{
		super("Get Ontologies for selected nodes", adapter.getCyApplicationManager(), "network", adapter.getCyNetworkViewManager());
		this.adapter = adapter;
		setPreferredMenu("Select");
	}

	public void actionPerformed(ActionEvent e)
	{
		final CyApplicationManager manager = adapter.getCyApplicationManager();
		final CyNetworkView networkView = manager.getCurrentNetworkView();
		final CyNetwork network = networkView.getModel();

		//TODO retrieve just selected nodes
		for(@SuppressWarnings("unused") CyNode node : network.getNodeList())
		{
			// TODO get selected nodes and retrieve their info
			// TODO use node info to query NCBO Ontology Recommender
		}

		networkView.updateView();
	}
}
